package transport;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {
	private String url	= null;
	private String user	= null;
	private String pass	= null;
	private Connection con	= null;
	private ResultSet rs	= null;

	public DB() {
		super();
	}	
	
	public DB(String url, String user, String pass) {
		super();
		this.url	= url;
		this.user	= user;
		this.pass	= pass;
	}

	private void connect() {
		if (this.url==null) return;
		if (this.user==null) return;
		if (this.pass==null) return;
		try {
			this.con = DriverManager.getConnection(this.url,this.user,this.pass);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			if (this.con != null) this.con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void reConnect() {
		this.close();
		this.connect();
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
		if (this.con!=null) this.reConnect();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
		if (this.con!=null) this.reConnect();
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
		if (this.con!=null) this.reConnect();
	}
	
	public ResultSet getRs() {
		return rs;
	}
	
	public ResultSet query(String sql) {
		if (this.con==null) this.connect();
		if (this.con==null) return null;
		try {
			//System.out.println(sql);
			this.rs = this.con.prepareStatement(sql).executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public int update(String sql) {
		if (this.con==null) this.connect();
		if (this.con==null) return 0;
		int cs	= 0;
		try {
			//System.out.println(sql);
			//int rs = this.stmt.executeQuery(sql);
			PreparedStatement pstmt = this.con.prepareStatement(sql);
			//pstmt.setString(1, "Кофе");
			cs	= pstmt.executeUpdate();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cs;
	}
	
	@Override
	public String toString() {
		return "DB url=" + url + ", user=" + user;
	}

}
