package transport;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddActionListener implements ActionListener {
	
	private JFrame frame = null;
	private JTextField SHNAMEField	= null;
	private JTextField FULLNAMEField	= null;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//System.out.println(arg0);
		String cmd	= arg0.getActionCommand();
		if (cmd == "AddFormOpen") {
			frame = new JFrame("Добавить Вид ТС");
			Container cp = frame.getContentPane();
			cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
			// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			JPanel SHNAMEPanel = new JPanel();
			SHNAMEPanel.setLayout(new BoxLayout(SHNAMEPanel, BoxLayout.X_AXIS));
			cp.add(SHNAMEPanel);
			JLabel SHNAMELabel = new JLabel(" Краткое наименование: ");
			SHNAMEPanel.add(SHNAMELabel);
			SHNAMEField = new JTextField();
			SHNAMEPanel.add(SHNAMEField);

			JPanel FULLNAMEPanel = new JPanel();
			FULLNAMEPanel.setLayout(new BoxLayout(FULLNAMEPanel, BoxLayout.X_AXIS));
			cp.add(FULLNAMEPanel);
			JLabel FULLNAMELabel = new JLabel(" Полное наименование: ");
			FULLNAMEPanel.add(FULLNAMELabel);
			FULLNAMEField = new JTextField();
			FULLNAMEPanel.add(FULLNAMEField);

			JButton buttonAdd = new JButton("Добавить");
			buttonAdd.setActionCommand("AddFormSet");
			cp.add(buttonAdd);
			ActionListener buttonAddListener = this;
			buttonAdd.addActionListener(buttonAddListener);

			frame.setPreferredSize(new Dimension(500, 100));
			frame.pack();
			frame.setVisible(true);
		}
		else {
			DB db = ViewVIDTC.getDB();
			db.update("INSERT INTO test.VIDTC (SHNAME, FULLNAME) VALUES (\""+SHNAMEField.getText()+"\",\""+FULLNAMEField.getText()+"\")");
			ViewVIDTC.viewUpdate();
			frame.dispose();
		}
	}

}
