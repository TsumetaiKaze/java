package transport;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ViewVIDTC {
	
	private static DB db	= null;
	private static DefaultTableModel model	= null;

	public static void view() {
		JFrame frame	= new JFrame("Виды ТС");
        Container cp	= frame.getContentPane(); 
        cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel mainPanel	= new JPanel();
        mainPanel.setLayout(new BorderLayout());        
        cp.add(mainPanel);

        int widthPanel	= 15;
        int heightPanel	= 30; 
        
        JPanel centerPanel	= new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.setPreferredSize(new Dimension(100, 100));
        mainPanel.add(centerPanel,BorderLayout.CENTER);
        
        JPanel northPanel	= new JPanel();
        northPanel.setPreferredSize(new Dimension(widthPanel,heightPanel));
        mainPanel.add(northPanel,BorderLayout.NORTH);

        JPanel southPanel	= new JPanel();
        southPanel.setPreferredSize(new Dimension(widthPanel,heightPanel));
        southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.X_AXIS));
        mainPanel.add(southPanel,BorderLayout.SOUTH);
        
        JPanel westPanel	= new JPanel();
        westPanel.setPreferredSize(new Dimension(widthPanel,heightPanel));
        mainPanel.add(westPanel,BorderLayout.WEST);

        JPanel eastPanel	= new JPanel();
        eastPanel.setPreferredSize(new Dimension(widthPanel,heightPanel));
        mainPanel.add(eastPanel,BorderLayout.EAST);

        JButton buttonAdd = new JButton("Добавить");
        buttonAdd.setActionCommand("AddFormOpen");
        northPanel.add(buttonAdd);
        ActionListener buttonAddListener = new AddActionListener();
        buttonAdd.addActionListener(buttonAddListener);         
        
        Vector<String> TabHeader = new Vector<String>();
        TabHeader.add("ИД");
        TabHeader.add("Краткое наименование");
        TabHeader.add("Полное наименование");
        model = new DefaultTableModel(TabHeader, 0);
        JTable tableFJs = new JTable();
        tableFJs.setModel(model);
        JScrollPane scrollPane = new JScrollPane(tableFJs);
        centerPanel.add(scrollPane);

        viewUpdate();
        
        frame.setPreferredSize(new Dimension(500, 500));
        
        frame.pack();
        frame.setVisible(true);
	}

	public static DB getDB() {
		if (db==null) db	= new DB("jdbc:mysql://localhost:3306/test","root","Admin2@19");
		return db;
	}

	public static void viewUpdate() {
		if (model==null) return;
        getDB();        
        ResultSet rs = db.query("SELECT * FROM test.VIDTC");
        if (rs!=null) {
        	try {
        		model.setRowCount(0);
				while (rs.next()) {
					Vector<String> newRow = new Vector<String>();
					newRow.add(String.valueOf(rs.getInt("VIDT")));
					newRow.add(rs.getString("SHNAME"));
					newRow.add(rs.getString("FULLNAME"));
					model.addRow(newRow);						
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }		
	}	
}
