package pdf;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;

public class PDF {

	public static void main(String[] args) {
		
        // load PDF document
        PDFDocument document = new PDFDocument();
        try {
			document.load(new File("/home/kaze/MyDevelop/Java/Второй ЛУРВ ИП Шаклеин ЕА № 194564 от 29.07.2019 и № 194682 от 01.08.2019.pdf"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // create renderer
        SimpleRenderer renderer = new SimpleRenderer();

        // set resolution (in DPI)
        renderer.setResolution(300);

        // render
        List<Image> images = null;
		try {
			images = renderer.render(document);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RendererException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        // write images to files to disk as PNG
        for (int i = 0; i < images.size(); i++) {
        	try {
				ImageIO.write((RenderedImage) images.get(i), "png", new File((i + 1) + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            }        
//		File f = new File("/home/kaze/MyDevelop/Java/Второй ЛУРВ ИП Шаклеин ЕА № 194564 от 29.07.2019 и № 194682 от 01.08.2019.pdf");
//	    PDDocument document	= null;
//		try {
//			document = PDDocument.load(f);
//		} catch (InvalidPasswordException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (document==null) return;
//	    PDFRenderer pdfRenderer = new PDFRenderer(document);
//	    for (int page = 0; page < document.getNumberOfPages(); ++page) {
//	        BufferedImage bim	= null;
//			try {
//				bim = pdfRenderer.renderImageWithDPI(
//				  page, 300, ImageType.RGB);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			if (bim==null) continue;
//	        try {
//				ImageIOUtil.writeImage(
//				  bim, String.format("src/pdf-%d.%s", page + 1, "png"), 300);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	    }
//	    try {
//	    	if (document!=null) document.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	   }

}
