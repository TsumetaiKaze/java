package dbObject;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Buyers")
public class Buyers {
    @Id
    @GeneratedValue 
    @Column(name = "id", nullable = false, insertable = true, updatable = true)	
    private int id;
    @Basic
    @Column(name = "first_name", nullable = false, insertable = true, updatable = true, length = 60)    
    private String firstName;
    @Basic
    @Column(name = "last_name", nullable = false, insertable = true, updatable = true, length = 40)    
    private String lastName;
 
    public Buyers() {
    }
     
    public Buyers(String lastName, String firstName) {
       this.lastName = lastName;
       this.firstName = firstName;
    }

    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        StringBuffer disc = new StringBuffer("id: ");
        disc.append("LastName: ");
        disc.append(getLastName());
        disc.append("FirstName");
        disc.append(getFirstName());
      return disc.toString();
    }
}
