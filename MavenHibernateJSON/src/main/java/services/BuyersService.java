package services;

import dao.BuyersDAO;
import dbObject.Buyers;

public class BuyersService {
    private BuyersDAO buyersDao = new BuyersDAO();

    public BuyersService() {
    }

    public Buyers findUser(int id) {
        return buyersDao.findById(id);
    }

    public void saveUser(Buyers buyer) {
    	buyersDao.save(buyer);
    }

    public void deleteUser(Buyers buyer) {
    	buyersDao.delete(buyer);
    }

    public void updateUser(Buyers buyer) {
    	buyersDao.update(buyer);
    }
}
