package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dbObject.Buyers;
import utils.HibernateSessionFactoryUtil;

public class BuyersDAO {

    public Buyers findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Buyers.class, id);
    }
    
    public void save(Buyers buyer) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(buyer);
        tx1.commit();
        session.close();
    }
    
    public void update(Buyers buyer) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(buyer);
        tx1.commit();
        session.close();
    }

    public void delete(Buyers buyer) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(buyer);
        tx1.commit();
        session.close();
    }
}
