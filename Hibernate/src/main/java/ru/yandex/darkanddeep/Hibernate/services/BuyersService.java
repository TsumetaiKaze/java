package ru.yandex.darkanddeep.Hibernate.services;

import ru.yandex.darkanddeep.Hibernate.Entity.Buyers;
import ru.yandex.darkanddeep.Hibernate.dao.BuyersDAO;

public class BuyersService {
    private BuyersDAO buyersDao = new BuyersDAO();

    public BuyersService() {
    }

    public Buyers findBuyer(int id) {
        return buyersDao.findById(id);
    }

    public void saveBuyer(Buyers buyer) {
    	buyersDao.save(buyer);
    }

    public void deleteBuyer(Buyers buyer) {
    	buyersDao.delete(buyer);
    }

    public void updateBuyer(Buyers buyer) {
    	buyersDao.update(buyer);
    }

}
