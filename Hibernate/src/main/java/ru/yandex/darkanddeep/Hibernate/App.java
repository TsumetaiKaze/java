package ru.yandex.darkanddeep.Hibernate;

import ru.yandex.darkanddeep.Hibernate.Entity.Buyers;
import ru.yandex.darkanddeep.Hibernate.services.BuyersService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
		BuyersService buyersService = new BuyersService();
		Buyers buyer = new Buyers("Иванова","Маша");
        buyersService.saveBuyer(buyer);        
    }
}
