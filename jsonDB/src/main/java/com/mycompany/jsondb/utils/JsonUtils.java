/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsondb.utils;

import com.mycompany.jsondb.operations.Operations;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

/**
 *
 * @author kaze
 */
public class JsonUtils {

    public static String makeJSONErr(String err) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "error");
        jsonObject.put("message", err);
        return jsonObject.toJSONString();
    }
    
    public static JSONObject readJSON(String inTxT) {
        try {
            return (JSONObject) JSONValue.parseWithException(inTxT);
        } catch (ParseException ex) {
            Operations.log("Error read incoming JSON "+ex.toString());
        }
        return null;
    }
    
}
