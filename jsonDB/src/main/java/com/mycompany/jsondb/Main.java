/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsondb;

import com.mycompany.jsondb.operations.Operations;
import java.io.File;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 *
 * @author kaze
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        String fileOutput = args[2];
        File fileOut = new File(fileOutput);
        if (!fileOut.exists()) {
            log.info("File outgoing data not exists will be used output.json");
            fileOutput = "output.json";
        }
        Operations.setFileOut(fileOutput);
        if (args.length < 3) {
            Operations.log("Need 3 argument: command, incoming data file, path for the outgoing data file");
            return;
        }
        File fileIn = new File(args[1]);
        if (!fileIn.exists()) {
            Operations.log("File incoming data not exists");
            return;
        }
        Operations.setCommand(args[0]);
        Operations.setFileIn(args[1]);
        Operations.executeOperation();
    }
}
