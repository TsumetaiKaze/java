/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsondb.operations;

import com.mycompany.jsondb.utils.JsonUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kaze
 */
public class Operations {

    private static final Logger log = LoggerFactory.getLogger(Operations.class);

    private static String command = null;
    private static String fileIn = null;
    private static String fileInTxT = null;
    private static String fileOut = null;
    private static LinkedList<HashMap> selectionParametersList = null;

    public static String getCommand() {
        return command;
    }

    public static void setCommand(String command) {
        Operations.command = command;
    }

    public static String getFileIn() {
        return fileIn;
    }

    public static void setFileIn(String fileIn) {
        Operations.fileIn = fileIn;
    }

    public static String getFileOut() {
        return fileOut;
    }

    public static void setFileOut(String fileOut) {
        Operations.fileOut = fileOut;
    }

    private static void saveOutput(String txt) {
        try {
            FileWriter fileOutput = new FileWriter(fileOut, false);
            fileOutput.write(txt);
            fileOutput.flush();
        } catch (IOException ex) {
            log.error("Error writing file out " + ex.toString());
        }
    }

    private static void saveErr(String txt) {
        String err = JsonUtils.makeJSONErr(txt);
        saveOutput(err);
    }

    public static void log(String txt) {
        log.error(txt);
        saveErr(txt);
    }

    private static void readFileIn() {
        Operations.fileInTxT = null;
        try {
            Operations.fileInTxT = FileUtils.readFileToString(new File(fileIn), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("Error writing file out " + ex.toString());
        }
    }

    private static boolean FileInIsEmpty() {
        if (fileInTxT == null) {
            log.error("File incoming data not read");
            return true;
        }
        if (fileInTxT.trim().length() == 0) {
            log.error("File incoming data is empty");
            return true;
        }
        return false;
    }

    private static void search() {
        readFileIn();
        if (FileInIsEmpty()) {
            return;
        }
        JSONObject inJSON = JsonUtils.readJSON(fileInTxT);
        if (inJSON == null) {
            log.error("Empty JSON Object");
            return;
        }
        JSONArray criterias = (JSONArray) inJSON.get("criterias");
        Iterator itCrit = criterias.iterator();
        if (selectionParametersList == null) {
            selectionParametersList = new LinkedList<>();
        } else {
            selectionParametersList.clear();
        }
        while (itCrit.hasNext()) {
            JSONObject criterion = (JSONObject) itCrit.next();
            Set keys = criterion.keySet();
            Iterator itkey = keys.iterator();
            HashMap<String, String> selectionParameters = new HashMap<>();
            while (itkey.hasNext()) {
                Object key = itkey.next();
                selectionParameters.put(key.toString(), criterion.get(key).toString());
            }
            if (selectionParameters.size() > 0) {
                selectionParametersList.addLast(selectionParameters);
            }
        }
        System.out.println(Operations.selectionParametersList);
    }

    private static void stat() {

    }

    public static void executeOperation() {
        switch (command) {
            case "search":
                search();
                break;
            case "stat":
                stat();
                break;
            default:
                log("The command was not identified");
        }
    }
}
