package ru.yandex.darkanddeep;

public class MusicPlayer {
    private Music music;

    //This is IoC
    public MusicPlayer(Music music) {
        this.music = music;
    }

    public void playMusic() {
        System.out.println(String.format("Playing: %s", music.getSong()));
    }
}
