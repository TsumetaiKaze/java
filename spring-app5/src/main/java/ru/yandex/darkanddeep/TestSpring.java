package ru.yandex.darkanddeep;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context  = new ClassPathXmlApplicationContext("applicationContext.xml");
//        Music music = context.getBean("musicBean", Music.class);
//        MusicPlayer musicPlayer = new MusicPlayer(music);
        MusicPlayer musicPlayer = context.getBean("musicPlayerBean",MusicPlayer.class);
        System.out.println(String.format("Name: %s",musicPlayer.getName()));
        musicPlayer.playMusic();
        System.out.println(String.format("Volume: %d",musicPlayer.getVolume()));
        context.close();
    }
}
