package pdfImg;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;

public class PDF_Img {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Need argument");
			return;
		}

		System.out.println(args[0]);

		File filePDF = new File(args[0]);

		if (!filePDF.exists()) {
			System.out.println("File not exists");
			return;
		}

		PDFDocument document = new PDFDocument();
		try {
			document.load(filePDF);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// create renderer
		SimpleRenderer renderer = new SimpleRenderer();

		// set resolution (in DPI)
		renderer.setResolution(300);

		// render
		List<Image> images = null;
		try {
			images = renderer.render(document);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RendererException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		if (images == null) {
			System.out.println("Image not found");
			return;
		}

		String dir = filePDF.getParent();
		String fileName = filePDF.getName();

		StringBuffer beginFilePath = new StringBuffer(dir);
		char ls = beginFilePath.charAt(beginFilePath.length() - 1);
		if (beginFilePath.indexOf("/") >= 0) {
			if (ls != '/')
				beginFilePath.append('/');
		} else {
			char slesh = (char) 92;
			if (ls != slesh)
				beginFilePath.append(slesh);
		}

		beginFilePath.append(fileName);
		beginFilePath.append('-');

		String ext = ".jpg";

		StringBuffer filePath = new StringBuffer();
		String filPathStr = null;

		// write images to files to disk as PNG
		for (int i = 0; i < images.size(); i++) {
			try {
				filePath.setLength(0);
				filePath.append(beginFilePath);
				filePath.append(i);
				filePath.append(ext);
				filPathStr = filePath.toString();

				File fileImg = new File(filPathStr);
				
				ImageIO.write((RenderedImage) images.get(i), "jpg", fileImg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
