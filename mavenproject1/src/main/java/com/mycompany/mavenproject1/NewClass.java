/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kaze
 */
public class NewClass {

    public static void main(String[] args) {
        System.out.println("Begin");
        Session session = null;

//		try {
        Configuration configuration = new Configuration();
        configuration = configuration.configure();
        configuration.addAnnotatedClass(Buyer.class);
        Properties prop = configuration.getProperties();

        StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();
        standardServiceRegistryBuilder.applySettings(prop);
//		standardServiceRegistryBuilder = standardServiceRegistryBuilder.configure("hibernate.cfg.xml");
        StandardServiceRegistry standardRegistry = standardServiceRegistryBuilder.build();

//					.configure("hibernate.cfg.xml").build();
//			Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
//			SessionFactory factory = metaData.getSessionFactoryBuilder().build();
        SessionFactory factory = configuration.buildSessionFactory(standardRegistry);

        session = factory.getCurrentSession();
//		} catch (Throwable th) {
//			System.err.println("Enitial SessionFactory creation failed" + th);
//			throw new ExceptionInInitializerError(th);
//		}

        session.getTransaction().begin();
        Buyer buyer = new Buyer("lastName", "firstName");
        session.saveOrUpdate(buyer);
        session.getTransaction().commit();
        session.close();
        System.out.println("End");
    }
}
