package ru.yandex.darkanddeep;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context  = new ClassPathXmlApplicationContext("applicationContext.xml");
//        Music music = context.getBean("musicBean", Music.class);
//        MusicPlayer musicPlayer = new MusicPlayer(music);
        MusicPlayer musicPlayer = context.getBean("musicPlayerBean",MusicPlayer.class);
        System.out.println(String.format("Name: %s",musicPlayer.getName()));
        musicPlayer.playMusic();
        System.out.println(String.format("Volume: %d",musicPlayer.getVolume()));
        MusicPlayer musicPlayer2 = context.getBean("musicPlayerBean",MusicPlayer.class);
        musicPlayer2.setName("moon");
        musicPlayer2.setVolume(1);
        System.out.println(String.format("Name: %s",musicPlayer2.getName()));
        musicPlayer2.playMusic();
        System.out.println(String.format("Volume: %d",musicPlayer2.getVolume()));
        System.out.println(musicPlayer==musicPlayer2);
        System.out.println(musicPlayer);
        System.out.println(musicPlayer2);
        context.close();
    }
}
