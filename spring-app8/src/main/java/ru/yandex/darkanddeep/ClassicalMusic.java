package ru.yandex.darkanddeep;

public class ClassicalMusic implements Music{
    
    private ClassicalMusic() {}
    
    public static ClassicalMusic getClassicalMusic() {
        return new ClassicalMusic();
    }
    
    public void init() {
        System.out.println("Initialization");
    }

    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
    
    public void destroy() {
        System.out.println("Destruction");
    }    
}
