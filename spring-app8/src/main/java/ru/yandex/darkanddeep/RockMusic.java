package ru.yandex.darkanddeep;

import org.springframework.stereotype.Component;

@Component ("musicRockBean")//можно добавить id к компоненту/бину иначе будет использовано название класса
public class RockMusic implements Music{
    @Override
    public String getSong() {
        return "Wind cries Mary";
    }
}
