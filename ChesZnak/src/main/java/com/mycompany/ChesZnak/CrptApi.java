/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */
package com.mycompany.ChesZnak;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Пелагеин Юрий
 */
public class CrptApi {
    
    private Date     startPeriod  = null;
    private int      requestLimit = 0;
    private int      requestCount = 0;
    private TimeUnit timeUnit     = null;
    private String   token        = "";
    private Date     startToken   = null;
    private int      tokenLimit   = 10;
    private String   info         = "";//инфополе куда помещаются данные о работе класса

    public CrptApi(TimeUnit timeUnit, int requestLimit) {
        this.requestLimit = requestLimit;
        if (null==timeUnit)
            this.timeUnit = TimeUnit.MILLISECONDS;
        else
            this.timeUnit = timeUnit;
    }
    
    public String getInfo() {
        return this.info;
    }

    public void ClearInfo() {
        this.info = "";//очищаем поле чтобы не путатся в старых и новых данных
    }
    
    public class DocIntroProd {
        public String   description = "";
        public String   doc_id = "";
        public String   doc_status = "";
        public String   doc_type = "LP_INTRODUCE_GOODS";
        public boolean  importRequest = true;
        public String   owner_inn = "";
        public String   participant_inn = "";
        public String   producer_inn = "";
        public String   production_date = "";
        public String   production_type = "";
        public Object[] products = null;
        public String   reg_date = "";
        public String   reg_number = "";
    }
    
    private String singData(String inData) {
        String outData = inData;
        //здесть данные подписываются криптопро
        return outData;
    }
    
    private String getToken() throws InterruptedException, IOException, ParseException {
        if (null!=this.startToken) {
            Date curMoment  = new Date();
            long duration   = curMoment.getTime() - this.startToken.getTime();
            duration = TimeUnit.MILLISECONDS.toHours(duration);
            if (duration<this.tokenLimit)
                return this.token;
        }
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://ismp.crpt.ru/api/v3/auth/cert/key"))
                .GET()
                .build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        this.info = this.info+response.body().toString();//заносим ответ на запрос чтобы потом его можно было проанализировать при необходимости. this чтобы явно отличать поле от переменной
        Object obj = new JSONParser().parse(response.body().toString());
        JSONObject j = (JSONObject) obj;
        String uuid = (String) j.get("uuid");
        String data = (String) j.get("data");
        data = singData(data);
        j = new JSONObject();
        j.put("uuid", uuid);
        j.put("data", data);
        request = HttpRequest.newBuilder()
                .uri(URI.create("https://ismp.crpt.ru/api/v3/auth/cert")) 
                .header("Content-Type", "application/json; charset=UTF-8") 
                .header("Accept", "application/json") 
                .POST(HttpRequest.BodyPublishers.ofString(j.toJSONString())) 
                .build();
        response = client.send(request, HttpResponse.BodyHandlers.ofString());
        this.info = this.info+response.body().toString();
        obj = new JSONParser().parse(response.body().toString());
        j = (JSONObject) obj;
        this.token = (String) j.get("token");
        this.startToken = new Date();
        return this.token;
    }
    
    private String DataToJSON(DocIntroProd inDataDoc) {
        /*здесь можно написать универсалный метод,
        который будет перебирать в цикле поля и методы
        чтобы получить набор значений и поместить его в json
        так мы сможем превратить в json любой объект не меняя код,
        но система получится менее прогнозируемой.
        Лучше, если каждый входящий объект документ
        будет иметь специальный метод сеарилизующий его в json.
        В рамках теста будем считать, что структура объекта документ известна и неизменна.*/
        JSONObject j = new JSONObject();
        j.put("description"    , inDataDoc.description);
        j.put("doc_id"         , inDataDoc.doc_id);
        j.put("doc_status"     , inDataDoc.doc_status);
        j.put("doc_type"       , inDataDoc.doc_type);
        j.put("importRequest"  , inDataDoc.importRequest);
        j.put("owner_inn"      , inDataDoc.owner_inn);
        j.put("participant_inn", inDataDoc.participant_inn);
        j.put("producer_inn"   , inDataDoc.producer_inn);
        j.put("production_date", inDataDoc.production_date);
        j.put("production_type", inDataDoc.production_type);
        j.put("products"       , inDataDoc.products);
        j.put("reg_date"       , inDataDoc.reg_date);
        j.put("reg_number"     , inDataDoc.reg_number);
        return j.toJSONString();
    }
    
    /* опеределяем промежуток времени между вызовами метода
    если вызов в первый раз, то сразу разрешаем и ставим отметку времени
    в зависимости от установленого периода определяем сколько прошло времени
    если ноль, то мы еще в пределах периода иначе просто сбрасываем счетчик и разрешаем запрос
    если мы еще в переделах периода и лимит не привышен разрешаем запрос
    в иных случаях возвращаем запрет
    */
    private boolean AbortRequest() {
        if (null==this.startPeriod) {
            this.startPeriod = new Date();
            return false;
        }
        Date curMoment  = new Date();
        long duration   = curMoment.getTime() - this.startPeriod.getTime();
        switch (this.timeUnit) {
            case SECONDS -> duration = TimeUnit.MILLISECONDS.toSeconds(duration);
            case MINUTES -> duration = TimeUnit.MILLISECONDS.toMinutes(duration);
            case HOURS   -> duration = TimeUnit.MILLISECONDS.toHours(duration);
            case DAYS    -> duration = TimeUnit.MILLISECONDS.toDays(duration);
        }
        if (duration==0) {
            if (this.requestCount<this.requestLimit) {
                this.requestCount++;
                return false;
            }
        }
        else {
            this.requestCount = 0;
            this.startPeriod = new Date();
            return false;
        }
        return true;
    }
    
    public void CreateDoc(DocIntroProd inDataDoc, String inSing) throws InterruptedException, IOException, ParseException {
        if (AbortRequest()) return;
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://ismp.crpt.ru/api/v3/lk/documents/create")) 
                .header("Content-Type", "application/json; charset=UTF-8") 
                .header("Accept", "application/json")
                .header("Authorization", "Bearer "+getToken())
                .POST(HttpRequest.BodyPublishers.ofString(DataToJSON(inDataDoc))) 
                .build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        this.info = this.info+response.body().toString();
    }
}