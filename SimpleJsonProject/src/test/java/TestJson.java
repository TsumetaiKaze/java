import org.junit.Before;
import org.junit.Test;
import ua.com.prologistic.app.JsonUtils;

import java.net.URL;

import static org.junit.Assert.assertTrue;

public class TestJson {
    public static final String WEATHER_URL =
            "http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=241de9349721df959d8800c12ca4f1f3";

    URL url;

    @Before
    public void init() {
        url = JsonUtils.createUrl(WEATHER_URL);
    }

    @Test
    public void testParseUrl() {
        String json = JsonUtils.parseUrl(url);
        assertTrue(json != null);
    }
}
