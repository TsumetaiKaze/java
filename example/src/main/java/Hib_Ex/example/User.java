package Hib_Ex.example;

public class User {

	private int id;
	
	private int age;
	
	private String firstname;
	private String lastname;
	
	public User() {}

	public User(int id) {
		this.id = id;
	}
	
	public User(String lastname, String firstname, int age) {
		this.lastname = lastname;
		this.firstname = firstname;
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", age=" + age + ", firstname=" + firstname + ", lastname=" + lastname + "]";
	}

}
