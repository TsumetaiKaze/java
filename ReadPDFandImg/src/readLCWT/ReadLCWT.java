package readLCWT;

import java.io.File;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class ReadLCWT {

	public static void main(String[] args) {
		File imageFile = new File("/home/kaze/MyDevelop/Java/Первый ЛУРВ ИП Шаклеин ЕА № 194564 от 29.07.2019 и № 194682 от 01.08.2019.pdf-0.png");
		ITesseract instance = new Tesseract(); // JNA Interface Mapping
		// ITesseract instance = new Tesseract1(); // JNA Direct Mapping
		instance.setDatapath("/home/kaze/MyDevelop/Java/ReadPDFandImg/src/tessdata/"); // replace <parentPath> with path to parent directory of tessdata
		instance.setLanguage("eng");
		String result	= null;
		try {
			result = instance.doOCR(imageFile);
		} catch (TesseractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(result);
	}

}
