package QR;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QR {

	public static void main(String[] args) {
		if (args.length<1) {
			System.out.println("Need argument");
			return;
		}		
		
		System.out.println(args[0]);
		
		File filePDF = new File(args[0]);

		if (!filePDF.exists()) {
			System.out.println("File not exists");
			return;
		}
		
		PDFDocument document = new PDFDocument();
		try {
			document.load(filePDF);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// create renderer
		SimpleRenderer renderer = new SimpleRenderer();

		// set resolution (in DPI)
		renderer.setResolution(300);

		// render
		List<Image> images = null;
		try {
			images = renderer.render(document);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RendererException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		if (images==null) {
			System.out.println("Image not found");
			return;
		}
		
		String dir		= filePDF.getParent();
		String fileName	= filePDF.getName();
		
		StringBuffer beginFilePath = new StringBuffer(dir);
	    char ls = beginFilePath.charAt(beginFilePath.length()-1);
	    if (beginFilePath.indexOf("/")>=0) {
	    	if (ls!='/') beginFilePath.append('/');
	    }else {
	    	char slesh = (char) 92;
	    	if (ls!=slesh) beginFilePath.append(slesh);
	    }		

	    beginFilePath.append(fileName);
	    beginFilePath.append('-');
	    
	    String ext	= ".png";
	    String txt	= ".txt";
	    
	    StringBuffer filePath	= new StringBuffer();
	    String filPathStr		= null;

	    StringBuffer fileTxtPath	= new StringBuffer();
	    String filTxtPathStr		= null;
	    
		String charset	= "UTF-8"; // or "ISO-8859-1"
		Map hintMap	= new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
	    
		// write images to files to disk as PNG
		for (int i = 0; i < images.size(); i++) {

			filePath.setLength(0);
        	filePath.append(beginFilePath);
        	filePath.append(i);
        	filePath.append(ext);
        	filPathStr	= filePath.toString();
        	
        	fileTxtPath.setLength(0);
        	fileTxtPath.append(beginFilePath);
        	fileTxtPath.append(i);
        	fileTxtPath.append(txt);
        	filTxtPathStr	= fileTxtPath.toString();         	
			
        	File fileImg = new File(filPathStr);
        	
			try {
				ImageIO.write((RenderedImage) images.get(i), "png", fileImg);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			
			FileWriter writer = null;					
			try {
				writer = new FileWriter(filTxtPathStr, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (writer==null) continue; 
			
			String qrValue	= null;
			try {
				qrValue = readQRCode(filPathStr, charset, hintMap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (qrValue==null) continue;
			
			try {
				writer.write(qrValue);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}			
			
		}
	}

	public static String readQRCode(String filePath, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		FileInputStream file	= new FileInputStream(filePath);
		BufferedImage img		= ImageIO.read(file);
		BufferedImageLuminanceSource imgSrc	= new BufferedImageLuminanceSource(img);
		HybridBinarizer bin			= new HybridBinarizer(imgSrc);
		BinaryBitmap binaryBitmap	= new BinaryBitmap(bin);
		MultiFormatReader mfr		= new MultiFormatReader();
		Result qrCodeResult			= mfr.decode(binaryBitmap,hintMap);
		return qrCodeResult.getText();
	}	
	
}