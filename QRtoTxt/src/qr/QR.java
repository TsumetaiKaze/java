package qr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QR {

	public static String readQRCode(String filePath, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		FileInputStream file	= new FileInputStream(filePath);
		BufferedImage img		= ImageIO.read(file);
		BufferedImageLuminanceSource imgSrc	= new BufferedImageLuminanceSource(img);
		HybridBinarizer bin			= new HybridBinarizer(imgSrc);
		BinaryBitmap binaryBitmap	= new BinaryBitmap(bin);
		MultiFormatReader mfr		= new MultiFormatReader();
		Result qrCodeResult			= mfr.decode(binaryBitmap,hintMap);
		return qrCodeResult.getText();
	}	
	
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Need argument");
			return;
		}

		System.out.println(args[0]);

		File fileImg = new File(args[0]);

		if (!fileImg.exists()) {
			System.out.println("File not exists");
			return;
		}
		
		String dir		= fileImg.getParent();
		String fileName	= fileImg.getName();
		
	    StringBuffer filePath	= new StringBuffer(dir);
	    char ls = filePath.charAt(filePath.length()-1);
	    if (filePath.indexOf("/")>=0) {
	    	if (ls!='/') filePath.append('/');
	    }else {
	    	char slesh = (char) 92;
	    	if (ls!=slesh) filePath.append(slesh);
	    }		
	    filePath.append(fileName);
	    filePath.append(".txt");
	    String filPathStr = filePath.toString();
				
		FileWriter writer = null;					
		try {
			writer = new FileWriter(filPathStr, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (writer==null) {
			System.out.println("Error create for writing "+filPathStr);
			return;
		} 
		
		String qrValue	= null;
		try {
			String charset	= "UTF-8"; // or "ISO-8859-1"
			Map hintMap	= new HashMap();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);			
			qrValue = readQRCode(args[0], charset, hintMap);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (qrValue==null) {
			System.out.println("Error get text from QR-code");
			return;
		}
		
		try {
			writer.write(qrValue);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}			
    	
	}

}
